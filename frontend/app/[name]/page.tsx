"use client";

import { usePathname } from "next/navigation";
import { useMutation, useQuery } from "@apollo/client";
import { GET_POKEMON_BY_NAME } from "../../lib/graphql/queries/pokemons";
import apolloClient from "../../apollo-client";
import LoadingSpinner from "../../components/UI/LoadingSpinner/LoadingSpinner";
import { Fragment, useState } from "react";
import { Pokemon } from "../../lib/utils/types";
import DetailView from "../../components/DetailView/DetailView";
import {
  SET_FAVORITE_POKEMON,
  UNSET_FAVORITE_POKEMON,
} from "../../lib/graphql/mutations/pokemons";

export default function PokemonDetailPage() {
  const pathName = usePathname().slice(1);
  const [pokemon, setPokemon] = useState<Pokemon>();

  const [setFavoritePokemon] = useMutation(SET_FAVORITE_POKEMON, {
    client: apolloClient,
  });
  const [unsetFavoritePokemon] = useMutation(UNSET_FAVORITE_POKEMON, {
    client: apolloClient,
  });

  const { loading } = useQuery(GET_POKEMON_BY_NAME, {
    client: apolloClient,
    variables: {
      name: pathName.toString(),
    },
    onCompleted: (res) => {
      if (!res) return null;
      setPokemon(res.pokemonByName);
    },
  });

  const toggleFavoriteStatusHandler = (id: any, favoriteState: boolean) => {
    const mutation = favoriteState ? unsetFavoritePokemon : setFavoritePokemon;

    mutation({
      variables: {
        id: id,
      },
      notifyOnNetworkStatusChange: true,
      onCompleted: () => {
        setPokemon((prevPokemon) => {
          if (!prevPokemon) return prevPokemon;
          return {
            ...prevPokemon,
            isFavorite: !prevPokemon.isFavorite,
          };
        });
      },
      onError: (e) => {
        console.error(e);
      },
    });
  };
  return (
    <Fragment>
      {loading && <LoadingSpinner />}
      {!!pokemon && (
        <DetailView
          pokemon={pokemon}
          toggleFavorite={toggleFavoriteStatusHandler}
        />
      )}
    </Fragment>
  );
}
