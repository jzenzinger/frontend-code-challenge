import LoadingSpinner from "../components/UI/LoadingSpinner/LoadingSpinner";

export default function Loading() {
  return <LoadingSpinner />;
}
