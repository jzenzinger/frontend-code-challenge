"use client";

import React, { Suspense, useEffect, useState } from "react";
import { useQuery } from "@apollo/client";
import apolloClient from "../apollo-client";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { router } from "next/client";
import {
  GET_ALL_POKEMONS,
  GET_POKEMON_TYPES,
} from "../lib/graphql/queries/pokemons";
import LoadingSpinner from "../components/UI/LoadingSpinner/LoadingSpinner";
import { useDebouncedCallback } from "use-debounce";
import styles from "../styles/globals.module.scss";
import NavigationSection from "../components/NavigationSection/NavigationSection";
import PokemonsSection from "../components/PokemonsSection/PokemonsSection";
import { Pokemon } from "../lib/utils/types";
import { Waypoint } from "react-waypoint";

export default function Page() {
  const [pokemons, setPokemons] = useState<Pokemon[]>([]);
  const [totalCountPokemons, setTotalCountPokemons] = useState(0);
  const [errorText, setErrorText] = useState("");
  const [filterType, setFilterType] = useState("");
  const [filterTypes, setFilterTypes] = useState({ pokemonTypes: [""] });
  const [showOnlyFavorites, setOnlyShowFavorites] = useState(false);

  const { replace } = useRouter();
  const searchParams = useSearchParams();
  const pathName = usePathname();
  const [searchQuery, setSearchQuery] = useState(
    searchParams.get("search")?.toString() || "",
  );
  const [isLoadingMore, setIsLoadingMore] = useState(false);
  const [showList, setShowList] = useState(false);

  const {
    loading,
    refetch: RefetchGetAllPokemonQuery,
    fetchMore,
  } = useQuery(GET_ALL_POKEMONS, {
    client: apolloClient,
    variables: {
      limit: 50,
      offset: 0,
      search: searchQuery,
      filter: {
        type: filterType,
        isFavorite: showOnlyFavorites,
      },
    },
    notifyOnNetworkStatusChange: true,
    onCompleted: (res) => {
      setErrorText("");
      if (!res.pokemons.edges.length) {
        setErrorText("Oh no, looks like your Pokemon doesn't exist! 😥");
      }
      setPokemons(res.pokemons.edges);
      setTotalCountPokemons(res.pokemons.count);
    },
  });

  useQuery(GET_POKEMON_TYPES, {
    client: apolloClient,
    onCompleted: (res) => {
      if (!res) return null;
      setFilterTypes(res);
    },
    notifyOnNetworkStatusChange: true,
  });

  const debouncedHandleSearch = useDebouncedCallback((value: string) => {
    setSearchQuery(value);
    RefetchGetAllPokemonQuery({ limit: 200, search: searchQuery });
  }, 300);

  const handleSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    debouncedHandleSearch(e.target.value);
  };

  const debouncedFilterChange = useDebouncedCallback((value: string) => {
    setFilterType(value);
    RefetchGetAllPokemonQuery({
      filter: { type: filterType, isFavorite: false },
      limit: 200,
      offset: 0,
    });
  }, 300);

  const handleFilterType = (e: React.ChangeEvent<HTMLOptionElement>) => {
    debouncedFilterChange(e.target.value);
  };

  const handleShowOnlyFavorites = (value: boolean) => {
    setOnlyShowFavorites(value);
    RefetchGetAllPokemonQuery({
      filter: {
        type: filterType,
        isFavorite: showOnlyFavorites,
      },
    });
  };

  const scrollHandler = async () => {
    setIsLoadingMore(true);
    await fetchMore({
      variables: { offset: pokemons.length },
    }).then((res) => {
      if (res!.data!.pokemons!.edges) {
        setPokemons((prevPokemons) => [
          ...prevPokemons,
          ...(res.data.pokemons.edges as Pokemon[]),
        ]);
      }
    });
    setIsLoadingMore(false);
  };

  useEffect(() => {
    const params = new URLSearchParams(searchParams);

    if (searchQuery) {
      params.set("search", searchQuery);
    } else {
      params.delete("search");
    }

    replace(`${pathName}?${params.toString()}`);
  }, [searchQuery, router]);

  let waypoint = null;
  if (!loading && !isLoadingMore && pokemons.length !== totalCountPokemons) {
    waypoint = <Waypoint topOffset={"1250px"} onEnter={scrollHandler} />;
  }

  return (
    <>
      <div className={styles.homepage}>
        <div className={styles.stickySection}>
          <NavigationSection
            filterByTypeHandler={handleFilterType}
            filterTypes={filterTypes}
            filterValue={filterType}
            searchValue={searchQuery}
            handleSearchValue={handleSearch}
            showOnlyFavorites={showOnlyFavorites}
            handleShowFavorites={handleShowOnlyFavorites}
          />
        </div>
        <Suspense fallback={<LoadingSpinner />}>
          <div className={styles.pokemonSection}>
            {!!errorText && <p>{errorText}</p>}
            <PokemonsSection pokemons={pokemons} setPokemons={setPokemons} />
            {waypoint}
          </div>
        </Suspense>
      </div>
    </>
  );
}
