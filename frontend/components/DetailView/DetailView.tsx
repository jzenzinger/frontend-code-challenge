import React, { useRef } from "react";
import { useRouter } from "next/navigation";
import { HiMiniSpeakerWave } from "react-icons/hi2";
import styles from "./detailview.module.scss";
import { Pokemon } from "../../lib/utils/types";
import { FaHeart, FaRegHeart } from "react-icons/fa";

interface DetailViewProps {
  pokemon: Pokemon;
  toggleFavorite: (id: any, favorite: boolean) => void;
}

const DetailView = ({ pokemon, toggleFavorite }: DetailViewProps) => {
  const { replace } = useRouter();

  const audioRef = useRef(null);

  const pokemonMaxCP = Math.min(100, (pokemon!.maxCP / 3000) * 100);
  const pokemonMaxHP = Math.min(100, (pokemon!.maxHP / 3000) * 100);

  const playAudio = () => {
    if (audioRef.current) {
      audioRef.current.play();
    }
  };

  const handleFavoriteToggle = (e: any) => {
    e.stopPropagation();
    toggleFavorite(pokemon.id, pokemon.isFavorite);
  };

  const goBack = () => {
    replace(`/`);
  };

  return (
    <div className={styles.view}>
      <button onClick={goBack}>Go Back</button>
      <div className={styles.detailView}>
        <div className={styles.imageContainer}>
          <img src={pokemon.image} alt={pokemon.name} />
          <button className={styles.bottomLeftButton}>
            <HiMiniSpeakerWave onClick={playAudio} />
          </button>
        </div>
        <div className={styles.text}>
          <h1>{pokemon.name.toUpperCase()}</h1>
          <p>{pokemon.types.join(", ")}</p>
          <button
            className={styles.iconButton}
            onClick={(e) => handleFavoriteToggle(e)}
          >
            {/*  Change icon based on isFavorite */}
            {pokemon.isFavorite ? <FaHeart /> : <FaRegHeart />}
          </button>
          <div className={styles.textSection}>
            <p>
              <strong>Classification:</strong> {pokemon.classification}
            </p>
            <p>
              <strong>Resistant:</strong> {pokemon.resistant.join(", ")}
            </p>
            <p>
              <strong>Weaknesses:</strong> {pokemon.weaknesses.join(", ")}
            </p>
            <br />
            {!!pokemon?.evolutions.length && (
              <p>
                <strong>Evolution:</strong>{" "}
                {pokemon.evolutions.map((item) => `${item.name}`)}
              </p>
            )}
            {!!pokemon?.evolutionRequirements?.name && (
              <p>
                <strong>Requirements:</strong>{" "}
                {pokemon.evolutionRequirements.name}
              </p>
            )}
          </div>
          <div
            className={styles.progressBar}
            style={{
              "--progress-width": `${pokemonMaxCP}%`,
              "--progress-color": "blue",
            }}
          />
          <div
            className={styles.progressBar}
            style={{
              "--progress-width": `${pokemonMaxHP}%`,
              "--progress-color": "red",
            }}
          />
          <div className={styles.statsSection}>
            <div className={styles.statItem}>
              <p>
                <strong>Weight</strong>
              </p>
              {pokemon.weight.minimum} - {pokemon.weight.maximum}
            </div>
            <div className={styles.statItem}>
              <p>
                <strong>Height</strong>
              </p>
              {pokemon.height.minimum} - {pokemon.height.maximum}
            </div>
          </div>
          <audio ref={audioRef} src={pokemon.sound} />
        </div>
      </div>
    </div>
  );
};

export default DetailView;
