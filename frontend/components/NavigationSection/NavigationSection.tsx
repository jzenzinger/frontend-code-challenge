import React, { Fragment } from "react";
import SearchBar from "../UI/Search/SearchBar";
import TypeFilter from "../UI/TypeFilter/TypeFilter";

interface NavigationSectionProps {
  filterByTypeHandler: (e: any) => void;
  filterTypes: { pokemonTypes: string[] };
  filterValue: string;
  searchValue: string;
  handleSearchValue: (e: any) => void;
  handleShowFavorites: (value: boolean) => void;
  showOnlyFavorites: boolean;
}

const NavigationSection = ({
  filterByTypeHandler,
  filterTypes,
  filterValue,
  searchValue,
  handleSearchValue,
  showOnlyFavorites, // To show which one is active
  handleShowFavorites,
}: NavigationSectionProps) => {
  return (
    <Fragment>
      <SearchBar
        placeholder={"Search Pokemons..."}
        value={searchValue}
        onChangeHandler={handleSearchValue}
      />
      <TypeFilter
        filterByTypeHandler={filterByTypeHandler}
        types={filterTypes}
        value={filterValue}
      />
      <button onClick={() => handleShowFavorites(true)}>Show Favorites</button>
      <button onClick={() => handleShowFavorites(false)}>Show All</button>
    </Fragment>
  );
};

export default NavigationSection;
