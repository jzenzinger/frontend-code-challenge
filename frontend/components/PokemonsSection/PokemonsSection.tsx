import React, { Dispatch, SetStateAction } from "react";
import Card from "../UI/Card/Card";
import styles from "./pokemonssection.module.scss";
import { Pokemon } from "../../lib/utils/types";
import { useMutation } from "@apollo/client";
import {
  SET_FAVORITE_POKEMON,
  UNSET_FAVORITE_POKEMON,
} from "../../lib/graphql/mutations/pokemons";
import apolloClient from "../../apollo-client";
import { useRouter } from "next/navigation";

interface PokemonsSectionProps {
  pokemons: Pokemon[];
  setPokemons: Dispatch<SetStateAction<Pokemon[]>>;
}

const PokemonsSection = ({ pokemons, setPokemons }: PokemonsSectionProps) => {
  const [setFavoritePokemon] = useMutation(SET_FAVORITE_POKEMON, {
    client: apolloClient,
  });
  const [unsetFavoritePokemon] = useMutation(UNSET_FAVORITE_POKEMON, {
    client: apolloClient,
  });
  const { replace } = useRouter();

  const handleShowDetailView = (pokemonName: string) => {
    replace(`/${pokemonName}`);
  };

  function toggleFavoriteStatusHandler(id: any, favoriteState: boolean) {
    const mutation = favoriteState ? unsetFavoritePokemon : setFavoritePokemon;

    mutation({
      variables: {
        id: id,
      },
      notifyOnNetworkStatusChange: true,
      onCompleted: () => {
        const pokemonIndex = pokemons.findIndex((pokemon) => pokemon.id === id);

        if (pokemonIndex !== -1) {
          const updatedPokemons: Pokemon[] = [...pokemons];

          updatedPokemons[pokemonIndex] = {
            ...updatedPokemons[pokemonIndex],
            isFavorite: !updatedPokemons[pokemonIndex].isFavorite,
          };

          setPokemons(updatedPokemons);
        }
      },
      onError: (e) => {
        console.error(e);
      },
    });
  }

  return (
    <div className={styles.cards}>
      {pokemons.map((item) => (
        <Card
          key={item.name.toLowerCase()}
          pokemon={item}
          openDetailHandler={handleShowDetailView}
          changeFavoriteHandler={toggleFavoriteStatusHandler}
        />
      ))}
    </div>
  );
};

export default PokemonsSection;
