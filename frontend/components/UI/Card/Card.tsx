import styles from "./card.module.scss";
import { FaHeart, FaRegHeart } from "react-icons/fa";
import { Pokemon } from "../../../lib/utils/types";

interface CardProps {
  pokemon: Pokemon;
  openDetailHandler: (pokemonName: string) => void;
  changeFavoriteHandler: (id: any, favoriteState: boolean) => void;
}

const Card = ({
  pokemon,
  openDetailHandler,
  changeFavoriteHandler,
}: CardProps) => {
  const handleFavoriteToggle = (e: any) => {
    e.stopPropagation();
    changeFavoriteHandler(pokemon.id, pokemon.isFavorite);
  };

  const detailHandler = () => {
    openDetailHandler(pokemon.name);
  };

  return (
    <div className={styles.card} onClick={detailHandler}>
      <div className={styles.imageContainer}>
        <img src={pokemon.image} alt="Card" className={styles.image} />
      </div>
      <div className={styles.content}>
        <div>
          <h6 className={styles.caption}>{pokemon.name}</h6>
          <p className={styles.text}>{pokemon.types.join(", ")}</p>
        </div>
        <button
          className={styles.iconButton}
          onClick={(e) => handleFavoriteToggle(e)}
        >
          {/*  Change icon based on isFavorite */}
          {pokemon.isFavorite ? <FaHeart /> : <FaRegHeart />}
        </button>
      </div>
    </div>
  );
};

export default Card;
