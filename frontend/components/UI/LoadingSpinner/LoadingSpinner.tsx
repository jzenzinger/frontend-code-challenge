import React from "react";
import styles from "./loadingspinner.module.scss";

const LoadingSpinner = () => {
  return <div className={styles.loading}></div>;
};

export default LoadingSpinner;
