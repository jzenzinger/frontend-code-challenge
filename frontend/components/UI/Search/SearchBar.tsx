import React from "react";
import styles from "./searchbar.module.scss";

interface SearchBarProps {
  onChangeHandler: (e: any) => void;
  placeholder: string;
  value: string;
}

const SearchBar = ({ value, placeholder, onChangeHandler }: SearchBarProps) => {
  return (
    <input
      type="text"
      className={styles.searchbar}
      placeholder={placeholder}
      onChange={onChangeHandler}
      defaultValue={value}
    />
  );
};

export default SearchBar;
