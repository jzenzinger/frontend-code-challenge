import React from "react";
import styles from "./searchbar.module.scss";

interface TabsProps {
  onChangeHandler: (e: any) => void;
  placeholder: string;
  value: string;
}

const Tabs = ({ value, placeholder, onChangeHandler }: TabsProps) => {
  return (
    <input
      type="text"
      className={styles.searchbar}
      placeholder={placeholder}
      onChange={onChangeHandler}
      defaultValue={value}
      autoFocus
    />
  );
};

export default Tabs;
