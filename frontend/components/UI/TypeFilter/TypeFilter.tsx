import React, { Fragment } from "react";
import styles from "./typefilter.module.scss";

interface TypeFilterProps {
  filterByTypeHandler: (e: any) => void;
  types: { pokemonTypes: string[] };
  value: string;
}

const TypeFilter = ({ filterByTypeHandler, types, value }: TypeFilterProps) => {
  return (
    <Fragment>
      {!!types.pokemonTypes && (
        <select
          value={value}
          onChange={filterByTypeHandler}
          className={styles.selector}
        >
          <option value="" selected>
            Type
          </option>
          {types.pokemonTypes.map((option) => (
            <option key={option} value={option} className={styles.option}>
              {option}
            </option>
          ))}
        </select>
      )}
    </Fragment>
  );
};

export default TypeFilter;
