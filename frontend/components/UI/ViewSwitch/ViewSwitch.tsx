import React from "react";
import styles from "./viewswitch.module.scss";

interface ViewSwitchProps {
  onChangeHandler: (e: any) => void;
  placeholder: string;
  value: string;
}

const ViewSwitch = ({
  value,
  placeholder,
  onChangeHandler,
}: ViewSwitchProps) => {
  return (
    <input
      type="text"
      className={styles.searchbar}
      placeholder={placeholder}
      onChange={onChangeHandler}
      defaultValue={value}
      autoFocus
    />
  );
};

export default ViewSwitch;
