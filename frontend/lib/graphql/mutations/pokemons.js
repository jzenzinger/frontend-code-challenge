import { gql } from "@apollo/client";

export const SET_FAVORITE_POKEMON = gql(`
  mutation SetFavoritePokemon($id: ID!) {
    favoritePokemon(id: $id) {
      id
      name
    }
  }
`);

export const UNSET_FAVORITE_POKEMON = gql(`
  mutation UnsetFavoritePokemon($id: ID!) {
    unFavoritePokemon(id: $id) {
      id
      name
    }
  }
`);
