import { gql } from "@apollo/client"; // For initial loading

// For initial loading, searching, filtering and also lazyLoading of other 50+ more pokemons
export const GET_ALL_POKEMONS = gql(`
  query GetAllPokemons(
  $limit: Int
  $offset: Int
  $search: String
  $filter: PokemonFilterInput
  ) {
    pokemons(
      query: { limit: $limit, offset: $offset, search: $search, filter: $filter }
    ) {
      edges {
        id
        name
        image
        number
        types
        isFavorite
      }
      count
    }
  }
`);

// When opening detailed view
export const GET_POKEMON_BY_NAME = gql(`
  query GetPokemonByName($name: String!) {
    pokemonByName(name: $name) {
      id
      name
      number
      types
      image
      sound
      resistant
      weight {
        minimum
        maximum
      }
      height {
        minimum
        maximum
      }
      classification
      weaknesses
      maxCP
      maxHP
      fleeRate
      isFavorite
      evolutions {
        id
        name
        image
      }
      evolutionRequirements {
        name
      }
      attacks {
        fast {
          name
          type
          damage
        }
        special {
          name
          type
          damage
        }
      }
    }
  }
`);

// For showing Pokemon types
export const GET_POKEMON_TYPES = gql(`
  query GetPokemonTypes {
    pokemonTypes
  }
`);
