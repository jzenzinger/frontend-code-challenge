export type PokemonConnection = {
  limit: number;
  offset: number;
  count: number;
  edges: [Pokemon];
};

export type Pokemon = {
  id: string;
  number: number;
  name: string;
  weight: PokemonDimension;
  height: PokemonDimension;
  classification: string;
  types: [string];
  resistant: [string];
  attacks: PokemonAttack;
  weaknesses: [string];
  fleeRate: number;
  maxCP: number;
  evolutions: [Pokemon];
  evolutionRequirements: PokemonEvolutionRequirement;
  maxHP: number;
  image: string;
  sound: string;
  isFavorite: boolean;
};

export type PokemonDimension = {
  minimum: string;
  maximum: string;
};

export type PokemonAttack = {
  fast: [Attack];
  special: [Attack];
};

export type Attack = {
  name: string;
  type: string;
  damage: number;
};

export type PokemonEvolutionRequirement = {
  amount: number;
  name: string;
};
